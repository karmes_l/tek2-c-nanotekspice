//
// Execution.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice/include
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Sun Feb 14 11:22:30 2016 Karmes Lionel
// Last update Sun Feb 28 23:15:44 2016 Karmes Lionel
//

#include "Execution.hpp"

bool  Execution::_Sigint = false;

// servait pour les tests
Execution::Execution(Circuit* circuit)
  :_Circuit(circuit) {
  if (_Circuit) {
    checkOutputsLink();
    setupSIGINT();
  }
}

Execution::Execution(const std::string& filename)
  :_Circuit(NULL) {
  nts::IParser				*parser = new Parser();

  try {
    loadCircuit(filename, parser);
  }
  catch (const std::exception& e) {
    delete parser;
    throw;
  }
  delete parser;
  if (_Circuit) {
    checkOutputsLink();
    setupSIGINT();
  }
}

Execution::~Execution() {
  if (_Circuit)
    delete _Circuit;
}

void				Execution::loadInputs(int ac, const char* avs[]) {
  for (int i = 2; i < ac; ++i)
    loadInput(avs[i]);
}

void				Execution::start() {
  std::vector<nts::IComponent *>	componentsInput;
  Component_input*			component;

  componentsInput = _Circuit->getComponentsInput();
  for (size_t i = 0, c = componentsInput.size(); i < c; ++i) {
    component = (Component_input *)componentsInput[i];
    if ((component->getTypeInput() == INPUT || component->getTypeInput() == CLOCK)
	&& component->getPinsInfo()[0].getValue() == nts::UNDEFINED)
      throw std::runtime_error("Input '" + component->getName() + "' is not initialized.");
  }
  _Circuit->Simulate();
  _Circuit->Display();
  readStdin();
}

void				Execution::readStdin()  {
  std::string	buff;
  bool		(Execution::* ptr[4])(const std::string&);
  size_t		i;

  // Le circuit est forcement loader
  // if (!_Circuit)
  //   return ;
  ptr[0] = &Execution::Simulate;
  ptr[1] = &Execution::Loop;
  ptr[2] = &Execution::Display;
  ptr[3] = &Execution::ChangeInputValue;
  std::cout << "> ";
  while (std::getline(std::cin, buff) && buff != "exit") {
    if (buff == "dump")
      Dump();
    else {
      for (i = 0; i < 4 && !((this->* ptr[i])(buff)); ++i);
      if (i == 4)
	std::cout << "Unknow command '" << buff << "'" << std::endl;
    }
    std::cout << "> ";
  }
}

bool				Execution::ChangeInputValue(const std::string& buff) {
  if (buff.find("=") == std::string::npos)
    return false;
  try {
    loadInput(buff);
  }
  catch (const std::exception& e) {
    std::cout << e.what() << std::endl;
  }
  return true;
}

bool				Execution::Simulate(const std::string& buff) {
  if (buff != "simulate")
    return false;
  _Circuit->turnOffElectricPower();
  _Circuit->Simulate();
  return true;
}

bool				Execution::Loop(const std::string& buff) {
  if (buff != "loop")
    return false;
  _Sigint = false;
  while (!_Sigint)
    (void)Simulate("simulate");
  return true;
}

bool				Execution::Display(const std::string& buff) {
  if (buff != "display")
    return false;
  _Circuit->Display();
  return true;
}

// peut pas être dans le tableau de Execution::*ptr car la méthode est const
void				Execution::Dump() const {
  _Circuit->Dump();
}

void				Execution::setupSIGINT() const {
  if (signal(SIGINT, Execution::catchSIGINT) == SIG_ERR)
    throw std::runtime_error("SIG_ERR while catching SIGINT");
}

void				Execution::catchSIGINT(int param) {
  (void)param;
  _Sigint = true;
}

void					Execution::loadInput(const std::string& av) {
  size_t	pos;
  std::string	name;
  std::string	pinValue_str;
  int		pinValue_int;
  AComponent*	componentInput;
  std::istringstream		iss;
  
  pos = av.find("=");
  if (pos == std::string::npos ||
      (name = av.substr(0, pos)) == "" || (pinValue_str = av.substr(pos + 1)) == "")
    throw std::runtime_error("Bad syntax '" + av + "', synthax : input_name=input_value");
  componentInput = (AComponent*)_Circuit->getComponent(name);
  if (!componentInput || componentInput->getType() != "input" ||
      (((Component_input*)componentInput)->getTypeInput() != INPUT && ((Component_input*)componentInput)->getTypeInput() != CLOCK))
    throw std::runtime_error("Component input '" + name + "' unknown");
  if (pinValue_str != "0" && pinValue_str != "1")
    throw std::runtime_error("Input value'" + pinValue_str + "' not valid (0 or 1)");
  iss.str(pinValue_str);
  iss >> pinValue_int;
  componentInput->setPinInfoValue(0, (nts::Tristate)pinValue_int);
}

void					Execution::checkOutputsLink() const {
  std::vector<nts::IComponent *>	componentsOutput;
  AComponent*				component;

  componentsOutput = _Circuit->getComponentsOutput();
  for (size_t i = 0, c = componentsOutput.size(); i < c; ++i) {
    component = (AComponent *)componentsOutput[i];
    if (component->getAComponentLink(0) == NULL)
      throw std::runtime_error("Output '" + component->getName() + "' is not linked to anything.");
  }
}

void					Execution::loadCircuit(const std::string& filename, nts::IParser* parser) {
  std::ifstream				ifs(filename.c_str());
  std::string				line;
  nts::t_ast_node			*root(NULL);
  int					i(1);
  
  try {
    if (!ifs.is_open())
      throw std::runtime_error("Error opening file : " + filename);
    while (getline(ifs, line)) {
      parser->feed(line);
      root = parser->createTree();
      if (root) {
	parser->parseTree(*root);
	((Parser*)parser)->deleteTree(root);
      }
      ++i;
    }
    ifs.close();
    if (((Parser *)parser)->getSection() != nts_section::LINKS)
      throw std::runtime_error("No Links section found");
    _Circuit = ((Parser *)parser)->getCircuit();
    ((Parser *)parser)->setCircuit(NULL); // deplace le Circuit*
  }
  catch (const std::exception& e) {
    std::cerr << "ligne :" << i << std::endl;
    if (ifs.is_open())
      ifs.close();
    if (root)
      ((Parser*)parser)->deleteTree(root);
    throw;
  }
}
