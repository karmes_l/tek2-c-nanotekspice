//
// main.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Mon Feb  8 23:37:45 2016 Karmes Lionel
// Last update Sat Feb 13 19:35:58 2016 Karmes Lionel
//

#include "nanotekspice.hpp"
#include <iostream>

int	main(int ac, char *av[]) {
  Component_4001	component_4001("toto"); // nor
  Component_4001	component_4001_2("tata");
  Component_input	component_input[6] = {
    Component_input("a"),
    Component_input("b"),
    Component_input("c"),
    Component_input("d"),
    Component_input("e"),
    Component_input("f")
  };


  component_4001.Dump();
  component_input[0].setPinInfoValue(component_input[0].getPinIndex(1), nts::FALSE);
  component_input[1].setPinInfoValue(component_input[1].getPinIndex(1), nts::FALSE);
  component_input[2].setPinInfoValue(component_input[2].getPinIndex(1), nts::FALSE);
  component_input[3].setPinInfoValue(component_input[3].getPinIndex(1), nts::FALSE);
  component_input[4].setPinInfoValue(component_input[4].getPinIndex(1), nts::FALSE);
  component_input[5].setPinInfoValue(component_input[4].getPinIndex(1), nts::TRUE);
  
  component_input[0].SetLink(1, *((nts::IComponent *)&component_4001), 1);
  component_input[1].SetLink(1, *((nts::IComponent *)&component_4001), 2);
  // pin3 = !(FALSE || FALSE) = TRUE
  component_4001.SetLink(3, *((nts::IComponent *)&component_4001), 13);  
  component_input[2].SetLink(1, *((nts::IComponent *)&component_4001), 5);
  component_input[3].SetLink(1, *((nts::IComponent *)&component_4001), 6);
  // pin4 = !(FALSE || FALSE) = TRUE
  component_4001.SetLink(4, *((nts::IComponent *)&component_4001), 12);
  // pin11 = !(TRUE || TRUE) = FALSE
  component_4001.SetLink(11, *((nts::IComponent *)&component_4001), 9);
  component_input[4].SetLink(1, *((nts::IComponent *)&component_4001), 8);
  // pin10 = !(FALSE || FALSE) = TRUE

  // Pas d'ordre dans le linkage
  component_4001_2.SetLink(1, *((nts::IComponent *)&component_4001), 10);
  component_4001_2.SetLink(2, *((nts::IComponent *)&component_input[5]), 1);

  component_4001.Compute(0); //pas obligé car le dernier output est component_4001_2
  component_4001_2.Compute(0);
  // std::cout << "pin pos 3 : " << component_4001.Compute(3) << std::endl;
  // component_input.setPinValue(component_input.getPinIndex(0), nts::FALSE);
  component_4001.Dump();
  component_4001_2.Dump();
  (void)ac;
  (void)av;

  return (0);
}
