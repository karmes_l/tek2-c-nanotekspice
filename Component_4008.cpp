//
// Component_4008.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 17:58:26 2016 Karmes Lionel
// Last update Sun Feb 28 20:17:06 2016 Karmes Lionel
//

// ADDER

#include "Component_4008.hpp"

Component_4008::Component_4008(const std::string& name)
  :AComponent("4008", name, 14) {
  this->_PinsIndex[1] = 0;
  this->_PinsIndex[2] = 1;
  this->_PinsIndex[3] = 2;
  this->_PinsIndex[4] = 3;
  this->_PinsIndex[5] = 4;
  this->_PinsIndex[6] = 5;
  this->_PinsIndex[7] = 6;
  this->_PinsIndex[9] = 7;
  this->_PinsIndex[10] = 8;
  this->_PinsIndex[11] = 9;
  this->_PinsIndex[12] = 10;
  this->_PinsIndex[13] = 11;
  this->_PinsIndex[14] = 12;
  this->_PinsIndex[15] = 13;
  initPinsInfo();
  //ne prend pas de paramètre après le name (si param risque de bug)
}
Component_4008::~Component_4008(void) {
}

// execute TOUT le component des pins EMETOR, les pins EMETOR peuvent s'exécuter plusieurs fois (je n'ai pas set le flag pin._Computed())
nts::Tristate Component_4008::Compute(size_t pinIndex) { // pin_num = pinIndex (afin de pas convertir en PinIndex puis en PinPos recursivement (Compute::Compute() demande un pinIndex)
  AComponent::Compute();
  ComputeEmetor(10, 7, 6, 9);
  ComputeEmetor(11, 5, 4, 14);
  ComputeEmetor(12, 3, 2, 14);
  ComputeEmetor(13, 1, 15, 14);
  return (this->_PinsInfo[pinIndex].getValue());
}

void			Component_4008::ComputeEmetor(size_t S, size_t A, size_t B, size_t C1) {
  char		res(0);
  nts::Tristate	in[3] = {this->_PinsInfo[this->_PinsIndex[A]].getValue(),
			 this->_PinsInfo[this->_PinsIndex[B]].getValue(),
			 this->_PinsInfo[this->_PinsIndex[C1]].getValue()};

  if (in[0] == nts::UNDEFINED || in[1] == nts::UNDEFINED || in[2] == nts::UNDEFINED)
    this->_PinsInfo[this->_PinsIndex[S]].setValue(nts::UNDEFINED);
  else {
    res = in[0] + in[1] + in[2]; // car nts::TRUE == 1 et nts::FALSE = 0
    this->_PinsInfo[this->_PinsIndex[S]].setValue((nts::Tristate)(res & 1));
    this->_PinsInfo[this->_PinsIndex[14]].setValue((nts::Tristate)((res >> 1) & 1)); // CO
  }
}

void			Component_4008::Dump() const {
  std::cout << "Type : " << _Type  << std::endl;
  AComponent::Dump();
}

void		Component_4008::initPinsInfo() {
  this->_PinsInfo[0].setTypePin(RECEPTOR);
  this->_PinsInfo[1].setTypePin(RECEPTOR);
  this->_PinsInfo[2].setTypePin(RECEPTOR);
  this->_PinsInfo[3].setTypePin(RECEPTOR);
  this->_PinsInfo[4].setTypePin(RECEPTOR);
  this->_PinsInfo[5].setTypePin(RECEPTOR);
  this->_PinsInfo[6].setTypePin(RECEPTOR);
  this->_PinsInfo[7].setTypePin(RECEPTOR);
  this->_PinsInfo[8].setTypePin(EMETOR);
  this->_PinsInfo[9].setTypePin(EMETOR);
  this->_PinsInfo[10].setTypePin(EMETOR);
  this->_PinsInfo[11].setTypePin(EMETOR);
  this->_PinsInfo[12].setTypePin(EMETOR);
  this->_PinsInfo[13].setTypePin(RECEPTOR);
}
