//
// Component_output.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Wed Feb 10 00:25:26 2016 Karmes Lionel
// Last update Sun Feb 28 22:42:02 2016 Karmes Lionel
//

// OUTPUT

#include "Component_output.hpp"

Component_output::Component_output(const std::string& name)
  :AComponent("output", name, 1) {
  this->_PinsInfo[0].setValue(nts::UNDEFINED);
  this->_PinsIndex[1] = 0;
  initPinsInfo();
  //ne prend pas de paramètre après le name (si param risque de bug)
}
Component_output::~Component_output(void) {
}

nts::Tristate Component_output::Compute(size_t pin_num) {
  (void)pin_num;
  AComponent::Compute();
  return this->_PinsInfo[0].getValue();
}

void			Component_output::Dump() const {
  std::cout << "Type : " << _Type << std::endl; // peut planter car écrit TOUJOURS sur std::cout, à changer si cette forme n'est pas necessaire
  AComponent::Dump();
}

void		Component_output::initPinsInfo() {
  this->_PinsInfo[0].setTypePin(RECEPTOR);
}
