//
// Component_4514.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 17:58:26 2016 Karmes Lionel
// Last update Sun Feb 28 22:41:52 2016 Karmes Lionel
//

// 4bits decoder

#include "Component_4514.hpp"

Component_4514::Component_4514(const std::string& name)
  :AComponent("4514", name, 22) {
  this->_PinsIndex[1] = 0;
  this->_PinsIndex[2] = 1;
  this->_PinsIndex[3] = 2;
  this->_PinsIndex[4] = 3;
  this->_PinsIndex[5] = 4;
  this->_PinsIndex[6] = 5;
  this->_PinsIndex[7] = 6;
  this->_PinsIndex[8] = 7;
  this->_PinsIndex[9] = 8;
  this->_PinsIndex[10] = 9;
  this->_PinsIndex[11] = 10;
  this->_PinsIndex[13] = 11;
  this->_PinsIndex[14] = 12;
  this->_PinsIndex[15] = 13;
  this->_PinsIndex[16] = 14;
  this->_PinsIndex[17] = 15;
  this->_PinsIndex[18] = 16;
  this->_PinsIndex[19] = 17;
  this->_PinsIndex[20] = 18;
  this->_PinsIndex[21] = 19;
  this->_PinsIndex[22] = 20;
  this->_PinsIndex[23] = 21;
  initPinsInfo();
  //ne prend pas de paramètre après le name (si param risque de bug)
}
Component_4514::~Component_4514(void) {
}

// execute TOUT le component des pins EMETOR, les pins EMETOR peuvent s'exécuter plusieurs fois (je n'ai pas set le flag pin._Computed())
nts::Tristate Component_4514::Compute(size_t pinIndex) { // pin_num = pinIndex (afin de pas convertir en PinIndex puis en PinPos recursivement (Compute::Compute() demande un pinIndex)
  unsigned int		data(0);
  size_t		in[4] = {2, 3, 21, 22};
  size_t		s[16] = {11, 9, 10, 8, 7, 6, 5, 4, 18, 17, 20, 19, 14, 13, 16, 15};

  AComponent::Compute();
  if (this->_PinsInfo[this->_PinsIndex[23]].getValue() == nts::TRUE || // INHIBIT ||
      this->_PinsInfo[this->_PinsIndex[1]].getValue() == nts::TRUE)   // STROBE
    for (size_t i = 0; i < 16; ++i)
      this->_PinsInfo[this->_PinsIndex[s[i]]].setValue(nts::FALSE);
  if (this->_PinsInfo[this->_PinsIndex[23]].getValue() == nts::FALSE && // NINHIBIT
      this->_PinsInfo[this->_PinsIndex[1]].getValue() == nts::TRUE) {   // STROBE
    for (size_t	i = 0; i < 4; ++i)
      if (this->_PinsInfo[this->_PinsIndex[in[i]]].getValue() == nts::TRUE)
	data |= 1 << i;
    this->_PinsInfo[this->_PinsIndex[s[data]]].setValue(nts::TRUE);
  }
  return (this->_PinsInfo[pinIndex].getValue());
}

void			Component_4514::Dump() const {
  std::cout << "Type : " << _Type  << std::endl;
  AComponent::Dump();
}

void		Component_4514::initPinsInfo() {
  this->_PinsInfo[0].setTypePin(RECEPTOR);
  this->_PinsInfo[1].setTypePin(RECEPTOR);
  this->_PinsInfo[2].setTypePin(RECEPTOR);
  this->_PinsInfo[3].setTypePin(EMETOR);
  this->_PinsInfo[4].setTypePin(EMETOR);
  this->_PinsInfo[5].setTypePin(EMETOR);
  this->_PinsInfo[6].setTypePin(EMETOR);
  this->_PinsInfo[7].setTypePin(EMETOR);
  this->_PinsInfo[8].setTypePin(EMETOR);
  this->_PinsInfo[9].setTypePin(EMETOR);
  this->_PinsInfo[10].setTypePin(EMETOR);
  this->_PinsInfo[11].setTypePin(EMETOR);
  this->_PinsInfo[12].setTypePin(EMETOR);
  this->_PinsInfo[13].setTypePin(EMETOR);
  this->_PinsInfo[14].setTypePin(EMETOR);
  this->_PinsInfo[15].setTypePin(EMETOR);
  this->_PinsInfo[16].setTypePin(EMETOR);
  this->_PinsInfo[17].setTypePin(EMETOR);
  this->_PinsInfo[18].setTypePin(EMETOR);
  this->_PinsInfo[19].setTypePin(RECEPTOR);
  this->_PinsInfo[20].setTypePin(RECEPTOR);
  this->_PinsInfo[21].setTypePin(RECEPTOR);
}
