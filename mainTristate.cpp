//
// main.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Mon Feb  8 23:37:45 2016 Karmes Lionel
// Last update Wed Feb 10 11:34:50 2016 Karmes Lionel
//

#include "nanotekspice.hpp"
#include <iostream>

int	main(int ac, char *av[]) {
  // Component_4001	component_4001("toto");
  // Component_input	component_input("tata");
  nts::Tristate	v1(nts::TRUE), v2(nts::FALSE), v3(nts::UNDEFINED);

  std::cout << !(v1  || v1) << std::endl;
  std::cout << !(v1  || v2) << std::endl;
  std::cout << !(v1  || v3) << std::endl;
  std::cout << !(v2  || v1) << std::endl;
  std::cout << !(v2  || v2) << std::endl;
  std::cout << !(v2  || v3) << std::endl;
  std::cout << !(v3  || v1) << std::endl;
  std::cout << !(v3  || v2) << std::endl;
  std::cout << !(v3  || v3) << std::endl;
  std::cout << "AND" << std::endl;
  std::cout << !(v1  && v1) << std::endl;
  std::cout << !(v1  && v2) << std::endl;
  std::cout << !(v1  && v3) << std::endl;
  std::cout << !(v2  && v1) << std::endl;
  std::cout << !(v2  && v2) << std::endl;
  std::cout << !(v2  && v3) << std::endl;
  std::cout << !(v3  && v1) << std::endl;
  std::cout << !(v3  && v2) << std::endl;
  std::cout << !(v3  && v3) << std::endl;
  // std::cout << component_4001 << std::endl;
  // component_input.setPinValue(component_input.getPinIndex(0), nts::FALSE);
  // std::cout << component_4001 << std::endl;
  // std::cout << component_input << std::endl;
  (void)ac;
  (void)av;

  return (0);
}
