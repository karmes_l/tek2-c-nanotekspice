//
// main.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Mon Feb  8 23:37:45 2016 Karmes Lionel
// Last update Sat Feb 13 13:58:29 2016 Karmes Lionel
//

#include "nanotekspice.hpp"
#include <iostream>

// Marche mal, abandon, si les links sont sur le même chipset cela peut echouer, comme ici (cela dépend de l'odre des links)

int	main(int ac, char *av[]) {
  Component_4001	component_4001("toto"); // nor
  Component_input	component_input[5] = {
    Component_input("a"),
    Component_input("b"),
    Component_input("c"),
    Component_input("d"),
    Component_input("e")
  };


  component_4001.Dump();
  component_input[0].setPinInfoValue(component_input[0].getPinIndex(1), nts::FALSE);
  component_input[1].setPinInfoValue(component_input[1].getPinIndex(1), nts::FALSE);
  component_input[2].setPinInfoValue(component_input[2].getPinIndex(1), nts::FALSE);
  component_input[3].setPinInfoValue(component_input[3].getPinIndex(1), nts::FALSE);
  component_input[4].setPinInfoValue(component_input[4].getPinIndex(1), nts::FALSE);
  
  component_4001.SetLink(11, *((nts::IComponent *)&component_4001), 1);
  component_input[0].SetLink(1, *((nts::IComponent *)&component_4001), 2);
  //pin3 = !(UNDEFINED || FALSE) = UNDEFINED POUR LE MOMENT, cela va etre re-compute() après
  component_input[1].SetLink(1, *((nts::IComponent *)&component_4001), 12);
  component_input[2].SetLink(1, *((nts::IComponent *)&component_4001), 13);
  //pin11 = !(FALSE || FALSE) = TRUE
  //pin3 re-compute() = !(TRUE || FALSE) = FALSE
  component_input[3].SetLink(1, *((nts::IComponent *)&component_4001), 8);
  component_4001.SetLink(3, *((nts::IComponent *)&component_4001), 9);
  //pin10 = !(FALSE || FALSE) = TRUE
  component_4001.SetLink(10, *((nts::IComponent *)&component_4001), 6);
  component_input[4].SetLink(1, *((nts::IComponent *)&component_4001), 5);
  // //pin4 = !(TRUE || FALSE) = FALSE

  component_4001.Compute(0);
  component_4001.Dump();
  (void)ac;
  (void)av;

  return (0);
}
