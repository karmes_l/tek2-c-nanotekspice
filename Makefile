##
## Makefile for  in /home/karmes_l/Projets/Maths
## 
## Made by lionel karmes
## Login   <karmes_l@epitech.net>
## 
## Started on  Mon Nov  3 16:51:51 2014 lionel karmes
## Last update Sun Feb 28 18:01:55 2016 Karmes Lionel
##

CC	= g++

RM	= rm -f

CXXFLAGS	+= -Wextra -Wall -Werror -std=gnu++11 -g3
CXXFLAGS	+= -I./include/

LDFLAGS	= -L. -lnanotekspice

NAMELIB	= libnanotekspice.a

SRCSLIB	= Parser.cpp \
	GenerateComponent.cpp \
	Execution.cpp \
	Circuit.cpp \
	AComponent.cpp \
	PinInfo.cpp \
	Component_4008.cpp \
	Component_terminal.cpp \
	Component_4514.cpp \
	Component_4040.cpp \
	Component_4013.cpp \
	Component_4503.cpp \
	Component_4081.cpp \
	Component_4071.cpp \
	Component_4069.cpp \
	Component_4030.cpp \
	Component_4011.cpp \
	Component_4001.cpp \
	Component_input.cpp \
	Component_output.cpp \
	Tristate.cpp

OBJSLIB	= $(SRCSLIB:.cpp=.o)

NAME	= nanotekspice

SRCS	= main.cpp

OBJS	= $(SRCS:.cpp=.o)


all: $(NAMELIB) $(NAME)

$(NAMELIB): $(OBJSLIB)
	ar -rc $(NAMELIB) $(OBJSLIB)
	ranlib $(NAMELIB)

$(NAME): $(OBJS)
	$(CC) $(OBJS) -o $(NAME) $(CXXFLAGS) $(LDFLAGS)
clean:
	$(RM) $(OBJS)
	$(RM) $(OBJSLIB)

fclean: clean
	$(RM) $(NAME)
	$(RM) $(NAMELIB)

re: fclean all


.PHONY: all clean fclean re
