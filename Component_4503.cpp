//
// Component_4503.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Sat Feb 27 11:53:40 2016 Karmes Lionel
// Last update Sat Feb 27 18:55:07 2016 Karmes Lionel
//

// Hex Non-Inverting 3-STATE Buffer

#include "Component_4503.hpp"

Component_4503::Component_4503(const std::string& name)
  :AComponent("4503", name, 14) {
  this->_PinsIndex[1] = 0;
  this->_PinsIndex[2] = 1;
  this->_PinsIndex[3] = 2;
  this->_PinsIndex[4] = 3;
  this->_PinsIndex[5] = 4;
  this->_PinsIndex[6] = 5;
  this->_PinsIndex[7] = 6;
  this->_PinsIndex[9] = 7;
  this->_PinsIndex[10] = 8;
  this->_PinsIndex[11] = 9;
  this->_PinsIndex[12] = 10;
  this->_PinsIndex[13] = 11;
  this->_PinsIndex[14] = 12;
  this->_PinsIndex[15] = 13;
  initPinsInfo();
  //ne prend pas de paramètre après le name (si param risque de bug)
}
Component_4503::~Component_4503(void) {
}

// execute TOUT le component des pins EMETOR, les pins EMETOR peuvent s'exécuter plusieurs fois (je n'ai pas set le flag pin._Computed())
nts::Tristate Component_4503::Compute(size_t pinIndex) { // pin_num = pinIndex (afin de pas convertir en PinIndex puis en PinPos recursivement (Compute::Compute() demande un pinIndex)
  AComponent::Compute();
  ComputeEmetor(3, 1, 2);
  ComputeEmetor(5, 1, 4);
  ComputeEmetor(7, 1, 6);
  ComputeEmetor(9, 1, 10);
  ComputeEmetor(11, 15, 12);
  ComputeEmetor(13, 15, 14);
  return (this->_PinsInfo[pinIndex].getValue());
}

void			Component_4503::ComputeEmetor(size_t OUT, size_t DIS_IN, size_t IN) {
  if (this->_PinsInfo[this->_PinsIndex[DIS_IN]].getValue() == nts::TRUE ||
      this->_PinsInfo[this->_PinsIndex[DIS_IN]].getValue() == nts::UNDEFINED)
    this->_PinsInfo[this->_PinsIndex[OUT]].setValue(nts::UNDEFINED);
  else
    this->_PinsInfo[this->_PinsIndex[OUT]].setValue(this->_PinsInfo[this->_PinsIndex[IN]].getValue());
}

void			Component_4503::Dump() const {
  std::cout << "Type : " << _Type  << std::endl;
  AComponent::Dump();
}

void		Component_4503::initPinsInfo() {
  this->_PinsInfo[0].setTypePin(RECEPTOR);
  this->_PinsInfo[1].setTypePin(RECEPTOR);
  this->_PinsInfo[2].setTypePin(EMETOR);
  this->_PinsInfo[3].setTypePin(RECEPTOR);
  this->_PinsInfo[4].setTypePin(EMETOR);
  this->_PinsInfo[5].setTypePin(RECEPTOR);
  this->_PinsInfo[6].setTypePin(EMETOR);
  this->_PinsInfo[7].setTypePin(EMETOR);
  this->_PinsInfo[8].setTypePin(RECEPTOR);
  this->_PinsInfo[9].setTypePin(EMETOR);
  this->_PinsInfo[10].setTypePin(RECEPTOR);
  this->_PinsInfo[11].setTypePin(EMETOR);
  this->_PinsInfo[12].setTypePin(RECEPTOR);
  this->_PinsInfo[13].setTypePin(RECEPTOR);
}
