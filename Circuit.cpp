//
// Circuit.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice/include
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Sat Feb 13 19:14:51 2016 Karmes Lionel
// Last update Sun Feb 28 23:11:05 2016 Karmes Lionel
//

#include "Circuit.hpp"

Circuit::Circuit() {  
}

Circuit::~Circuit() {
  for (size_t i = 0, c = _ComponentsInput.size(); i < c; ++i)
    delete _ComponentsInput[i];
  for (size_t i = 0, c = _Components.size(); i < c; ++i)
    delete _Components[i];
  for (size_t i = 0, c = _ComponentsOutput.size(); i < c; ++i)
    delete _ComponentsOutput[i];
}


const std::vector<nts::IComponent *>	Circuit::getComponentsInput() const {
  return _ComponentsInput;
}

const std::vector<nts::IComponent *>	Circuit::getComponents() const {
  return _Components;
}

const std::vector<nts::IComponent *>	Circuit::getComponentsOutput() const {
  return _ComponentsOutput;
}

nts::IComponent*			Circuit::getComponent(const std::string& name) const {
  std::vector<nts::IComponent *>::const_iterator	it;
  AComponent*		component;

  for (it = _ComponentsInput.begin(); it != _ComponentsInput.end(); ++it) {
    component = (AComponent*)*it;
    if (component->getName() == name)
      return *it;
  }
  for (it = _Components.begin(); it != _Components.end(); ++it) {
    component = (AComponent*)*it;
    if (component->getName() == name)
      return *it;
  }
  for (it = _ComponentsOutput.begin(); it != _ComponentsOutput.end(); ++it) {
    component = (AComponent*)*it;
    if (component->getName() == name)
      return *it;
  }
  return NULL;
}

void				Circuit::addComponents(nts::IComponent* iComponent) {
  AComponent*		component((AComponent *)iComponent);

  if ((component)->getType() == "input")
    _ComponentsInput.push_back(iComponent);
  else if ((component)->getType() == "output")
    _ComponentsOutput.push_back(iComponent);
  else
    _Components.push_back(iComponent);
}

void				Circuit::setComponentInputValue(const std::string &name, nts::Tristate value) {
  Component_input*	input;
  bool			find(false);

  for (std::vector<nts::IComponent *>::iterator it = _ComponentsInput.begin(); it != _ComponentsInput.end(); ++it) {
    input = (Component_input*)*it;
    if (input->getTypeInput() == INPUT && input->getName() == name) {
      input->setPinInfoValue(0, value);
      find = true;
    }
  }
  if (!find)
    std::cerr << "Input component with name '" << name << "' not found" << std::cerr;
}


void				Circuit::Simulate() {
  Component_input*	input;

  for (std::vector<nts::IComponent *>::iterator it = _ComponentsOutput.begin(); it != _ComponentsOutput.end(); ++it) {
    (void)(*it)->Compute(0);
  }
  // En Bonus : Re-Compute() les pin Emetor, (pas fais exprès), a enlever si cela pose probleme par la suite
  for (std::vector<nts::IComponent *>::iterator it = _Components.begin(); it != _Components.end(); ++it) {
    ((AComponent*)(*it))->setLatche(false);
    (void)(*it)->Compute(0);
  }
  turnOffElectricPower(); // Afin de limiter les erreurs (ex: mandion.nts) toujours le problème du flag _Computed lié à la récursivité
  for (std::vector<nts::IComponent *>::iterator it = _ComponentsOutput.begin(); it != _ComponentsOutput.end(); ++it) {
    (void)(*it)->Compute(0);
  }
  // En Bonus : Re-Compute() les pin Emetor, (pas fais exprès), a enlever si cela pose probleme par la suite
  for (std::vector<nts::IComponent *>::iterator it = _Components.begin(); it != _Components.end(); ++it) {
    (void)(*it)->Compute(0);
  }
  for (std::vector<nts::IComponent *>::iterator it = _ComponentsInput.begin(); it != _ComponentsInput.end(); ++it) {
    input = (Component_input*)*it;
    if (input->getTypeInput() == CLOCK)
      input->setPinInfoValue(0, !input->getPinsInfo()[0].getValue());
  }
}

// à arranger si j'ai le temps
void				Circuit::turnOffElectricPower() {
  AComponent*	component;
  std::vector<PinInfo>	pinsInfo;

  for (std::vector<nts::IComponent *>::iterator it = _ComponentsInput.begin(); it < _ComponentsInput.end(); ++it) {
    component = (AComponent*)*it;
    pinsInfo = component->getPinsInfo();
    for (size_t i = 0, c = pinsInfo.size(); i < c; ++i)
      if (pinsInfo[i].getComputed() != nts::UNDEFINED)
	component->setPinInfoComputed(i, nts::FALSE);
  }
  for (std::vector<nts::IComponent *>::iterator it = _Components.begin(); it < _Components.end(); ++it) {
    component = (AComponent*)*it;
    pinsInfo = component->getPinsInfo();
    for (size_t i = 0, c = pinsInfo.size(); i < c; ++i)
      if (pinsInfo[i].getComputed() != nts::UNDEFINED)
	component->setPinInfoComputed(i, nts::FALSE);
  }
  for (std::vector<nts::IComponent *>::iterator it = _ComponentsOutput.begin(); it < _ComponentsOutput.end(); ++it) {
    component = (AComponent*)*it;
    pinsInfo = component->getPinsInfo();
    for (size_t i = 0, c = pinsInfo.size(); i < c; ++i)
      if (pinsInfo[i].getComputed() != nts::UNDEFINED)
	component->setPinInfoComputed(i, nts::FALSE);
  }
}

void				Circuit::Display() {
  Component_output*		output;

  std::sort(_ComponentsOutput.begin() , _ComponentsOutput.end(), Circuit::sortByName);
  for (std::vector<nts::IComponent *>::iterator it = _ComponentsOutput.begin(); it != _ComponentsOutput.end(); ++it) {
    output = (Component_output*)*it;
    std::cout << output->getName() << "=" << (output->getPinsInfo()[0].getValue() == nts::TRUE ? "1" :
					      (output->getPinsInfo()[0].getValue() == nts::FALSE ? "0" :
					       "U")) << std::endl;
  }
}

void				Circuit::Dump() const {
  for (size_t i = 0, c = _ComponentsInput.size(); i < c; ++i)
    _ComponentsInput[i]->Dump();
  for (size_t i = 0, c = _Components.size(); i < c; ++i)
    _Components[i]->Dump();
  for (size_t i = 0, c = _ComponentsOutput.size(); i < c; ++i)
    _ComponentsOutput[i]->Dump();
}


bool			        Circuit::sortByName(const nts::IComponent* a, const nts::IComponent* b) {

  if (((AComponent*)a)->getName().compare(((AComponent*)b)->getName()) < 0)
    return true;
  return false;
}
