//
// Component_terminal.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 17:58:26 2016 Karmes Lionel
// Last update Sun Feb 28 22:30:35 2016 Karmes Lionel
//

// TERMINAL

#include "Component_terminal.hpp"

Component_terminal::Component_terminal(const std::string& name)
  :AComponent("terminal", name, 9), _Mem(nts::UNDEFINED) {
  this->_PinsIndex[1] = 0;
  this->_PinsIndex[2] = 1;
  this->_PinsIndex[3] = 2;
  this->_PinsIndex[4] = 3;
  this->_PinsIndex[5] = 4;
  this->_PinsIndex[6] = 5;
  this->_PinsIndex[7] = 6;
  this->_PinsIndex[8] = 7;
  this->_PinsIndex[9] = 8;
  initPinsInfo();
  //ne prend pas de paramètre après le name (si param risque de bug)
}
Component_terminal::~Component_terminal(void) {
}

// execute TOUT le component des pins EMETOR, les pins EMETOR peuvent s'exécuter plusieurs fois (je n'ai pas set le flag pin._Computed())
nts::Tristate Component_terminal::Compute(size_t pinIndex) { // pin_num = pinIndex (afin de pas convertir en PinIndex puis en PinPos recursivement (Compute::Compute() demande un pinIndex)
  char		c(0);

  AComponent::Compute();
  for (size_t i = 0; i < 8; ++i)
    if (this->_PinsInfo[i].getValue() == nts::TRUE)
      c |= 1 << (7 - i); // car 8bits, de gauche à droite
  if (!this->_Latche) {
    if (_Mem == nts::FALSE && this->_PinsInfo[8].getValue() == nts::TRUE)
      std::cerr << c;
    _Mem = this->_PinsInfo[8].getValue();
  }
  this->_Latche = true;
  return (this->_PinsInfo[pinIndex].getValue());
}

void			Component_terminal::Dump() const {
  std::cout << "Type : " << _Type  << std::endl;
  AComponent::Dump();
}

void		Component_terminal::initPinsInfo() {
  this->_PinsInfo[0].setTypePin(RECEPTOR);
  this->_PinsInfo[1].setTypePin(RECEPTOR);
  this->_PinsInfo[2].setTypePin(RECEPTOR);
  this->_PinsInfo[3].setTypePin(RECEPTOR);
  this->_PinsInfo[4].setTypePin(RECEPTOR);
  this->_PinsInfo[5].setTypePin(RECEPTOR);
  this->_PinsInfo[6].setTypePin(RECEPTOR);
  this->_PinsInfo[7].setTypePin(RECEPTOR);
  this->_PinsInfo[8].setTypePin(RECEPTOR);
}
