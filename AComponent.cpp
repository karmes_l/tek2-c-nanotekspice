//
// AComponent.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Mon Feb  8 23:48:05 2016 Karmes Lionel
// Last update Sun Feb 28 13:44:12 2016 Karmes Lionel
//

#include "AComponent.hpp"

AComponent::AComponent(const std::string& type, const std::string& name, size_t nbrPin)
  :_Type(type), _Name(name), _Latche(true), _ComponentsLink(nbrPin), _PinsIndexLink(nbrPin),
   _PinsInfo(nbrPin)
{
  resetLink();
}

AComponent::~AComponent() {
  for (size_t i = 0, c = _ComponentsLink.size(); i < c; ++i)
    if (_ComponentsLink[i] && _PinsIndexLink[i] != (size_t) -1)
      ((AComponent*)_ComponentsLink[i])->setAComponentLink_PinIndexLink(NULL, _PinsIndexLink[i], -1);
}

const std::string&		AComponent::getType() const {
  return _Type;
}

const std::string&		AComponent::getName() const {
  return _Name;
}

void				AComponent::setLatche(bool state) {
  _Latche = state;
}

const nts::IComponent*		AComponent::getAComponentLink(size_t pinIndex) const {
  return _ComponentsLink[pinIndex];
}

size_t				AComponent::getPinIndexLink(size_t pinIndex) const {
  return _PinsIndexLink[pinIndex];
}

void				AComponent::setAComponentLink_PinIndexLink(nts::IComponent* componentLink,
									 size_t pinIndex, size_t pinIndexLink) {
  _PinsIndexLink[pinIndex] = pinIndexLink;
  _ComponentsLink[pinIndex] = componentLink;
}

const std::vector<PinInfo>&	AComponent::getPinsInfo() const {
  return _PinsInfo;
}

void				AComponent::setPinInfoComputed(size_t pinIndex, nts::Tristate computed) {
  _PinsInfo[pinIndex].setComputed(computed);
}

void				AComponent::setPinInfoValue(size_t pinIndex, nts::Tristate value) {
  _PinsInfo[pinIndex].setValue(value);
}

const std::map<size_t, size_t>&	AComponent::getPinsIndex() const {
  return _PinsIndex;
}

// execute TOUT le component des pins RECEPTOR, execute les pins 1 à 1 si les pin._Computed == FALSE
void	AComponent::Compute() {
  for (size_t i = 0, c = _PinsInfo.size(); i < c; ++i) {
    // ATTENTION à ne pas compute les pins emetor sinon, les composants s'apelleront à l'infine : this.emetor -> componentLink.receptor qui, componentLink.receptor-> this.emetor
    // si la pin n'a pas de valeur, on descend recursivement le circuit et on remplie les pins du circuit
    if (_PinsInfo[i].getTypePin() == RECEPTOR &&
	// peut être plus obligé
	_PinsInfo[i].getComputed() == nts::FALSE) {
      _PinsInfo[i].setComputed(nts::TRUE);
      _PinsInfo[i].setValue(_ComponentsLink[i]->Compute(_PinsIndexLink[i]));
    }
  }
}

// pin_num = pinPos
void		AComponent::SetLink(size_t pin_num_this, nts::IComponent& component,
				   size_t pin_num_target) {
  AComponent	*componentLink = (AComponent*)&component;
  size_t	pinIndex_this(getPinIndex(pin_num_this));
  size_t	pinIndex_component(componentLink->getPinIndex(pin_num_target));
  std::ostringstream str;

  // si les 2pinTypes sont les mêmes
  if (_PinsInfo[pinIndex_this].getTypePin() == componentLink->getPinsInfo()[pinIndex_component].getTypePin()) {
    str << "Links between 1 pinType : " << _PinsInfo[pinIndex_this].getTypePin()
	<<" and 1 pinType " << componentLink->getPinsInfo()[pinIndex_component].getTypePin();
    throw std::invalid_argument(str.str());
  }  // si 1 des pins est déjà occupé (lié avec un autre pin)
  else if (getAComponentLink(pinIndex_this) != NULL || componentLink->getAComponentLink(pinIndex_component) != NULL)
    throw std::invalid_argument("Links with 1 pin always link");
  setAComponentLink_PinIndexLink(&component, pinIndex_this, pinIndex_component);
  _PinsInfo[pinIndex_this].setComputed(nts::FALSE);
  componentLink->setAComponentLink_PinIndexLink(this, pinIndex_component, pinIndex_this);
  componentLink->setPinInfoComputed(pinIndex_component, nts::FALSE);
}

void		AComponent::Dump() const {
  std::cout << "----------------------------------------" << std::endl;
  std::cout << *this << std::endl;
  std::cout << "----------------------------------------" << std::endl;
}

size_t		AComponent::getPinIndex(size_t pinPos) const {
  std::map<size_t, size_t>::const_iterator	it;
  std::ostringstream str;

  if ((it = _PinsIndex.find(pinPos)) != _PinsIndex.end())
    return (it->second);
  str << pinPos;
  throw std::invalid_argument("pinPos : " + str.str());
}

// Attention ne remet pas _PinInfo à l'état par défault
void		AComponent::resetLink() {
  for (size_t i = 0, c = _ComponentsLink.size(); i < c; ++i) {
    _ComponentsLink[i] = NULL;
    _PinsIndexLink[i] = -1;
  }
}



std::ostream&	operator<<(std::ostream& os, const AComponent& component) {
  const std::vector<PinInfo> pinsInfo = component.getPinsInfo();
  const std::map<size_t, size_t>      pinsIndex = component.getPinsIndex();

  os << "name variable : " << component.getName() << std::endl;
  os << "pins Info : " << std::endl;
  for (std::map<size_t, size_t>::const_iterator it = pinsIndex.begin(); it != pinsIndex.end(); ++it) {
    os << "pin Pos : " << it->first << "  ";
    os << pinsInfo[it->second] << std::endl;
  }
  return (os);
}
