//
// main.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Mon Feb  8 23:37:45 2016 Karmes Lionel
// Last update Thu Feb 11 19:31:36 2016 Karmes Lionel
//

#include "nanotekspice.hpp"
#include <iostream>

int	main(int ac, char *av[]) {
  Component_4001	component_4001("toto"); // nor
  Component_input	component_input("tata");
  Component_input	component_input2("tutu");
  Component_input	component_input3("tata");
  Component_input	component_input4("tutu");

  // component_4001.setPinValue(component_4001.getPinIndex(1), nts::TRUE);
  // std::cout << component_input << std::endl;
  component_4001.Dump();
  // !(FALSE || FALSE) = TRUE
  component_input.setPinInfoValue(component_input.getPinIndex(1), nts::FALSE);
  component_input2.setPinInfoValue(component_input.getPinIndex(1), nts::FALSE);
  // !(TRUE || UNDEFINED) = FALSE
  component_input3.setPinInfoValue(component_input.getPinIndex(1), nts::TRUE);
  component_input4.setPinInfoValue(component_input.getPinIndex(1), nts::UNDEFINED);
  component_input.SetLink(1, *((nts::IComponent *)&component_4001), 1);
  component_input2.SetLink(1, *((nts::IComponent *)&component_4001), 2);
  component_input3.SetLink(1, *((nts::IComponent *)&component_4001), 8);
  component_input4.SetLink(1, *((nts::IComponent *)&component_4001), 9);
  std::cout << "COMPUTE INPUT : " << component_4001.Compute(component_4001.getPinIndex(10)) << std::endl;
  std::cout << "COMPUTE INPUT : " << component_4001.Compute(component_4001.getPinIndex(3)) << std::endl;
  // std::cout << "pin pos 3 : " << component_4001.Compute(3) << std::endl;
  // component_input.setPinValue(component_input.getPinIndex(0), nts::FALSE);
  component_4001.Dump();
  component_input.Dump();
  // std::cout << component_input << std::endl;
  (void)ac;
  (void)av;

  return (0);
}
