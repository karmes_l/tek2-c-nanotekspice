//
// Tristate.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 12:32:59 2016 Karmes Lionel
// Last update Sun Feb 28 22:29:13 2016 Karmes Lionel
//

#include "Tristate.hpp"
#include <iostream>

nts::Tristate	operator&&(nts::Tristate a, nts::Tristate b) {
  if (a == nts::FALSE || b == nts::FALSE)
    return (nts::FALSE);
  else if (a == nts::UNDEFINED)
    return (nts::UNDEFINED);
  return (b);
}

nts::Tristate	operator||(nts::Tristate a, nts::Tristate b) {
  if (a == nts::TRUE || b == nts::TRUE)
    return (nts::TRUE);
  else if (a == nts::UNDEFINED)
    return (nts::UNDEFINED);
  return (b);
}

nts::Tristate	operator^(nts::Tristate a, nts::Tristate b) {
  if (a == nts::UNDEFINED || b == nts::UNDEFINED)
    return (nts::UNDEFINED);
  else if (a == b)
    return (nts::FALSE);
  return (nts::TRUE);
}

nts::Tristate	operator!(nts::Tristate a) {
  if (a == nts::TRUE)
    return nts::FALSE;
  else if (a == nts::FALSE)
    return nts::TRUE;
  return nts::UNDEFINED;
}

std::ostream&	operator<<(std::ostream& os, nts::Tristate tristate) {
  if (tristate == nts::UNDEFINED)
    os << "UNDEFINED";
  else if (tristate == nts::TRUE)
    os << "TRUE";
  else
    os << "FALSE";
  return (os);
}
