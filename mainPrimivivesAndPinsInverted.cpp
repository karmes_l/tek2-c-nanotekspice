//
// main.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Mon Feb  8 23:37:45 2016 Karmes Lionel
// Last update Sat Feb 13 16:59:08 2016 Karmes Lionel
//

#include "nanotekspice.hpp"
#include <iostream>

// Marche bien

int	main(int ac, char *av[]) {
  Component_4001	component_4001("toto"); // nor
  Component_input	component_input[5] = {
    Component_input("a"),
    Component_input("b"),
    Component_input("c"),
    Component_input("d"),
    Component_input("e")
  };


  component_4001.Dump();
  component_input[0].setPinInfoValue(component_input[0].getPinIndex(1), nts::FALSE);
  component_input[1].setPinInfoValue(component_input[1].getPinIndex(1), nts::FALSE);
  component_input[2].setPinInfoValue(component_input[2].getPinIndex(1), nts::FALSE);
  component_input[3].setPinInfoValue(component_input[3].getPinIndex(1), nts::FALSE);
  component_input[4].setPinInfoValue(component_input[4].getPinIndex(1), nts::FALSE);
  
  component_4001.SetLink(1, *((nts::IComponent *)&component_input[0]), 1);
  component_4001.SetLink(2, *((nts::IComponent *)&component_input[1]), 1);
  // pin3 = !(FALSE || FALSE) = TRUE
  component_4001.SetLink(13, *((nts::IComponent *)&component_4001), 3);
  component_4001.SetLink(5, *((nts::IComponent *)&component_input[2]), 1);
  component_4001.SetLink(6, *((nts::IComponent *)&component_input[3]), 1);
  // pin4 = !(FALSE || FALSE) = TRUE
  component_4001.SetLink(12, *((nts::IComponent *)&component_4001), 4);
  // pin11 = !(TRUE || TRUE) = FALSE
  component_4001.SetLink(9, *((nts::IComponent *)&component_4001), 11);
  component_4001.SetLink(8, *((nts::IComponent *)&component_input[4]), 1);
  // pin10 = !(FALSE || FALSE) = TRUE

  component_4001.Compute(0);
  component_4001.Dump();
  (void)ac;
  (void)av;

  return (0);
}
