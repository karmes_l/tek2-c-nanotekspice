//
// GenerateComponent.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice/include
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Thu Feb 18 16:39:16 2016 Karmes Lionel
// Last update Sun Feb 28 18:05:55 2016 Karmes Lionel
//

# include "GenerateComponent.hpp"

GenerateComponent::GenerateComponent() {
}

GenerateComponent::~GenerateComponent() {
}

nts::IComponent*	GenerateComponent::createComponent(const std::string& type, const std::string& value) {
  std::string		arr[17] {"input", "clock", "false", "true", "output", "4001", "4011", "4030", "4069", "4071", "4081", "4503", "4013", "4040", "4514", "terminal", "4008"};
  nts::IComponent*	(GenerateComponent::* ptr[17])(const std::string&) const;
  size_t		i(0);

  ptr[0] = &GenerateComponent::createInput;
  ptr[1] = &GenerateComponent::createClock;
  ptr[2] = &GenerateComponent::createFalse;
  ptr[3] = &GenerateComponent::createTrue;
  ptr[4] = &GenerateComponent::createOutput;
  ptr[5] = &GenerateComponent::create4001;
  ptr[6] = &GenerateComponent::create4011;
  ptr[7] = &GenerateComponent::create4030;
  ptr[8] = &GenerateComponent::create4069;
  ptr[9] = &GenerateComponent::create4071;
  ptr[10] = &GenerateComponent::create4081;
  ptr[11] = &GenerateComponent::create4503;
  ptr[12] = &GenerateComponent::create4013;
  ptr[13] = &GenerateComponent::create4040;
  ptr[14] = &GenerateComponent::create4514;
  ptr[15] = &GenerateComponent::createTerminal;
  ptr[16] = &GenerateComponent::create4008;
  for (; i < 17 && arr[i] != type; ++i);
  if (i != 17)
    return (GenerateComponent().* ptr[i])(value);
  return NULL;
}

nts::IComponent*	GenerateComponent::createInput(const std::string& value) const {
  return new Component_input(value, INPUT);
}

nts::IComponent*	GenerateComponent::createClock(const std::string& value) const {
  return new Component_input(value, CLOCK);
}

nts::IComponent*	GenerateComponent::createFalse(const std::string& value) const {
  return new Component_input(value, FALSE);
}

nts::IComponent*	GenerateComponent::createTrue(const std::string& value) const {
  return new Component_input(value, TRUE);
}

nts::IComponent*	GenerateComponent::createOutput(const std::string& value) const {
  return new Component_output(value);
}

nts::IComponent*	GenerateComponent::create4001(const std::string& value) const {
  return new Component_4001(value);
}

nts::IComponent*	GenerateComponent::create4011(const std::string& value) const {
  return new Component_4011(value);
}

nts::IComponent*	GenerateComponent::create4030(const std::string& value) const {
  return new Component_4030(value);
}

nts::IComponent*	GenerateComponent::create4069(const std::string& value) const {
  return new Component_4069(value);
}

nts::IComponent*	GenerateComponent::create4071(const std::string& value) const {
  return new Component_4071(value);
}

nts::IComponent*	GenerateComponent::create4081(const std::string& value) const {
  return new Component_4081(value);
}

nts::IComponent*	GenerateComponent::create4503(const std::string& value) const {
  return new Component_4503(value);
}

nts::IComponent*	GenerateComponent::create4013(const std::string& value) const {
  return new Component_4013(value);
}

nts::IComponent*	GenerateComponent::create4040(const std::string& value) const {
  return new Component_4040(value);
}

nts::IComponent*	GenerateComponent::create4514(const std::string& value) const {
  return new Component_4514(value);
}

nts::IComponent*	GenerateComponent::createTerminal(const std::string& value) const {
  return new Component_terminal(value);
}

nts::IComponent*	GenerateComponent::create4008(const std::string& value) const {
  return new Component_4008(value);
}
