//
// Component_4001.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 17:58:26 2016 Karmes Lionel
// Last update Sun Feb 21 16:31:42 2016 Karmes Lionel
//

// NOR

#include "Component_4001.hpp"

Component_4001::Component_4001(const std::string& name)
  :AComponent("4001", name, 12) {
  this->_PinsIndex[1] = 0;
  this->_PinsIndex[2] = 1;
  this->_PinsIndex[3] = 2;
  this->_PinsIndex[4] = 3;
  this->_PinsIndex[5] = 4;
  this->_PinsIndex[6] = 5;
  this->_PinsIndex[8] = 6;
  this->_PinsIndex[9] = 7;
  this->_PinsIndex[10] = 8;
  this->_PinsIndex[11] = 9;
  this->_PinsIndex[12] = 10;
  this->_PinsIndex[13] = 11;
  initPinsInfo();
  //ne prend pas de paramètre après le name (si param risque de bug)
}
Component_4001::~Component_4001(void) {
}

// execute TOUT le component des pins EMETOR, les pins EMETOR peuvent s'exécuter plusieurs fois (je n'ai pas set le flag pin._Computed())
nts::Tristate Component_4001::Compute(size_t pinIndex) { // pin_num = pinIndex (afin de pas convertir en PinIndex puis en PinPos recursivement (Compute::Compute() demande un pinIndex)
  AComponent::Compute();
  ComputeEmetor(3, 1, 2);
  ComputeEmetor(4, 5, 6);
  ComputeEmetor(10, 8, 9);
  ComputeEmetor(11, 12, 13);
  return (this->_PinsInfo[pinIndex].getValue());
}

void			Component_4001::ComputeEmetor(size_t Y, size_t A, size_t B) {
  
  this->_PinsInfo[this->_PinsIndex[Y]].setValue(!(this->_PinsInfo[this->_PinsIndex[A]].getValue() || this->_PinsInfo[this->_PinsIndex[B]].getValue()));

}

void			Component_4001::Dump() const {
  std::cout << "Type : " << _Type  << std::endl;
  AComponent::Dump();
}

void		Component_4001::initPinsInfo() {
  this->_PinsInfo[0].setTypePin(RECEPTOR);
  this->_PinsInfo[1].setTypePin(RECEPTOR);
  this->_PinsInfo[2].setTypePin(EMETOR);
  this->_PinsInfo[3].setTypePin(EMETOR);
  this->_PinsInfo[4].setTypePin(RECEPTOR);
  this->_PinsInfo[5].setTypePin(RECEPTOR);
  this->_PinsInfo[6].setTypePin(RECEPTOR);
  this->_PinsInfo[7].setTypePin(RECEPTOR);
  this->_PinsInfo[8].setTypePin(EMETOR);
  this->_PinsInfo[9].setTypePin(EMETOR);
  this->_PinsInfo[10].setTypePin(RECEPTOR);
  this->_PinsInfo[11].setTypePin(RECEPTOR);
}
