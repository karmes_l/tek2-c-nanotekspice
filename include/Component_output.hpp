//
// Component_output.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Wed Feb 10 00:26:41 2016 Karmes Lionel
// Last update Sat Feb 13 23:17:10 2016 Karmes Lionel
//

#ifndef COMPONENT_OUTPUT_HPP
# define COMPONENT_OUTPUT_HPP

# include "AComponent.hpp"

class   Component_output : public AComponent
{
public:
  Component_output(const std::string& name); //value = name = nom de la variable (+paramètre entre parenthèse)
  virtual	~Component_output();
  

  virtual nts::Tristate Compute(size_t pin_num);
  virtual void			Dump() const; // appelle dump classe mère
  virtual void		initPinsInfo();
};

#endif // COMPONENT_OUTPUT_HPP
