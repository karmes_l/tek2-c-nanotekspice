//
// AComponent.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Mon Feb  8 23:44:14 2016 Karmes Lionel
// Last update Sun Feb 28 12:58:49 2016 Karmes Lionel
//

#ifndef ACOMPONENT_HPP
# define ACOMPONENT_HPP

# include <string>
# include <vector>
# include <map>
# include <stdexcept>
# include <sstream>
# include "PinInfo.hpp"

class   AComponent : public nts::IComponent
{
protected:
  std::string	_Type; // type de component
  std::string	_Name; // nom de la variable
  bool		_Latche; // utile pour les composent ayant une mémoire interne (block la mémoire interne si = true)
  std::vector<IComponent *>		_ComponentsLink; // lien avec les autres components
  std::vector<size_t>			_PinsIndexLink; // pinIndex du component lié
  std::vector<PinInfo>		_PinsInfo; // info sur ces propres pins
  std::map<size_t, size_t>	_PinsIndex; // retourne l'index des vectors en fonction de la pin (la position d'une pin sur un chipset n'a pas forcement le même index des vectors)

public:
  AComponent(const std::string& type, const std::string& name, size_t nbrPin);
  virtual	~AComponent();

  const std::string&		getType() const;
  const std::string&		getName() const;
  void				setLatche(bool state);
  const nts::IComponent*	getAComponentLink(size_t pinIndex) const; // retourne le component lié
  size_t			getPinIndexLink(size_t pinIndex) const; // retourne le pinIndex du pin du component lié
  // @component : le composent lié à this
  // @pinIndex : le pinIndex du this qui est lié à @component
  // @pinIndexLink : le pinIndex du @component lié à this
  void				setAComponentLink_PinIndexLink(nts::IComponent* componentLink,
							      size_t pinIndex,  size_t pinIndexLink);
  const std::vector<PinInfo>&	getPinsInfo() const;
  void				setPinInfoComputed(size_t pinIndex, nts::Tristate); // set _Computed de _PinInfo[pinIndex], car getPinsInfo() retourne une constante
  void				setPinInfoValue(size_t pinIndex, nts::Tristate value); // set un Pin en fonction de l'index des vector
  const std::map<size_t, size_t>&	getPinsIndex() const;

  // virtual nts::Tristate			Compute(size_t pinIndexBegin);
  void				Compute();
  virtual void			SetLink(size_t pin_num_this, nts::IComponent& component,
					size_t pin_num_target);
  virtual void			Dump() const;
  size_t			getPinIndex(size_t pinPos) const; // retourne l'index des vectors d'un pin

protected:
  virtual void			initPinsInfo() = 0; // peut être en virtual
private:
  void				resetLink();
};

std::ostream&	operator<<(std::ostream& os, const AComponent& component);

#endif // ACOMPONENT_HPP
