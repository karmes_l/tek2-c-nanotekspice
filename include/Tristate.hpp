//
// Tristate.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 12:28:54 2016 Karmes Lionel
// Last update Sun Feb 21 00:13:08 2016 Karmes Lionel
//

#ifndef TRISTATE_HPP
# define TRISTATE_HPP

# include "IComponent.hpp"
# include <iostream>


nts::Tristate	operator&&(nts::Tristate a, nts::Tristate b);
nts::Tristate	operator||(nts::Tristate a, nts::Tristate b);
nts::Tristate	operator^(nts::Tristate a, nts::Tristate b);
nts::Tristate	operator!(nts::Tristate a);
std::ostream&	operator<<(std::ostream& os, nts::Tristate tristate);

#endif // TRISTATE_HPP
