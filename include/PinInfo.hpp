//
// PinInfo.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 22:37:11 2016 Karmes Lionel
// Last update Sun Feb 28 22:44:23 2016 Karmes Lionel
//

#ifndef PININFO_HPP
# define PININFO_HPP

# include "Tristate.hpp"

// a rajouter dans la class si la porté ne poses pa de problème
enum	TypePin {
  EMETOR,
  RECEPTOR
};

class   PinInfo
{
  nts::Tristate	_Value;
  TypePin	_TypePin;
  //@_Computed peut être remplacer par un boolean car les component ont un @_ComputedPins, si @_ComputedPins == FALSE alors @_Computed == UNDEFINED ou FALSE, sinon @_Computed == UNDEFINED ou TRUE (le UNDEFINED ne sert plus il peut être remplacer par TRUE), je garde un Tristate au cas où
  // @UNDEFINED : en dehors du circuit (ne pas compute)
  // @FALSE : dans le circuit, pas encore compute (à compute)
  // @TRUE : dans le circuit, déjà compute (ne pas compute)
  nts::Tristate	_Computed;
public:
  PinInfo();
  PinInfo(nts::Tristate value, TypePin typePin);
  ~PinInfo();

  nts::Tristate			getValue() const;
  void				setValue(nts::Tristate value);
  TypePin			getTypePin() const;
  void				setTypePin(TypePin typePin);
  nts::Tristate			getComputed() const;
  void				setComputed(nts::Tristate computed);
};

std::ostream&	operator<<(std::ostream& os, TypePin typePin);
std::ostream&	operator<<(std::ostream& os, const PinInfo& pinInfo);

#endif // PININFO_HPP
