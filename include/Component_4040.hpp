//
// Component_4040.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Sat Feb 27 11:54:00 2016 Karmes Lionel
// Last update Sun Feb 28 13:26:59 2016 Karmes Lionel
//

#ifndef COMPONENT_4040_HPP
# define COMPONENT_4040_HPP

# include "AComponent.hpp"

class   Component_4040 : public AComponent
{
  int	counter;

public:
  Component_4040(const std::string& name); //value = name = nom de la variable (+paramètre entre parenthèse)
  virtual	~Component_4040();
  

  virtual nts::Tristate Compute(size_t pin_num);
  virtual void			Dump() const;
  virtual void		initPinsInfo();
};

#endif // COMPONENT_4040_HPP
