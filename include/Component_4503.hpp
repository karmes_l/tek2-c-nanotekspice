//
// Component_4503.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Sat Feb 27 11:54:00 2016 Karmes Lionel
// Last update Sat Feb 27 11:54:04 2016 Karmes Lionel
//

#ifndef COMPONENT_4503_HPP
# define COMPONENT_4503_HPP

# include "AComponent.hpp"

class   Component_4503 : public AComponent
{
public:
  Component_4503(const std::string& name); //value = name = nom de la variable (+paramètre entre parenthèse)
  virtual	~Component_4503();
  

  virtual nts::Tristate Compute(size_t pin_num);
  void		        ComputeEmetor(size_t Y, size_t A, size_t B);
  virtual void			Dump() const;
  virtual void		initPinsInfo();
};

#endif // COMPONENT_4503_HPP
