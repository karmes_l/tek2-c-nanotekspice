//
// IComponent.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice/include
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Mon Feb  8 23:32:45 2016 Karmes Lionel
// Last update Wed Feb 17 22:18:04 2016 Karmes Lionel
//

#ifndef __ICOMPONENT_HPP__
# define __ICOMPONENT_HPP__

# include <stddef.h>

namespace nts
{
  enum	Tristate
    {
      UNDEFINED = (-true),
      TRUE = true,
      FALSE = false
    };

  class	IComponent
  {
  public:
    virtual nts::Tristate Compute(size_t pin_num = 1) = 0;
    // virtual nts::Tristate Compute(void) = 0;

    virtual void SetLink(size_t pin_num_this, nts::IComponent& component,
			 size_t pin_num_target) = 0;
    virtual void Dump(void) const = 0;
    virtual ~IComponent(void) { }
  };
}

#endif // __ICOMPONENT_HPP__
