//
// Component_4013.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Sat Feb 27 11:54:00 2016 Karmes Lionel
// Last update Sat Feb 27 19:10:01 2016 Karmes Lionel
//

#ifndef COMPONENT_4013_HPP
# define COMPONENT_4013_HPP

# include "AComponent.hpp"

class   Component_4013 : public AComponent
{
public:
  Component_4013(const std::string& name); //value = name = nom de la variable (+paramètre entre parenthèse)
  virtual	~Component_4013();
  

  virtual nts::Tristate Compute(size_t pin_num);
  void			ComputeEmetor(size_t Q, size_t NQ, size_t CL, size_t R, size_t D, size_t S);
  virtual void			Dump() const;
  virtual void		initPinsInfo();
};

#endif // COMPONENT_4013_HPP
