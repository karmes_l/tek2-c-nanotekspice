//
// Circuit.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice/include
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Sat Feb 13 18:31:11 2016 Karmes Lionel
// Last update Sun Feb 28 23:12:12 2016 Karmes Lionel
//

#ifndef CIRCUIT_HPP
# define CIRCUIT_HPP

# include "Component_input.hpp"
# include "Component_output.hpp"
# include <algorithm>

class	Circuit
{
  std::vector<nts::IComponent *>	_ComponentsInput; // possiblité de mettre les TRUE ou FALSE dans _Components (afin d'enlever ces flags de TypeInput)
  std::vector<nts::IComponent *>	_Components;
  std::vector<nts::IComponent *>	_ComponentsOutput;  // trier par name des composents output

public:
  Circuit();
  ~Circuit();

  
  const std::vector<nts::IComponent *>	getComponentsInput() const;
  const std::vector<nts::IComponent *>	getComponents() const ; // USELESS
  const std::vector<nts::IComponent *>	getComponentsOutput() const;
  nts::IComponent*			getComponent(const std::string& name) const; // cherche dans les 3 vectors en fonction de AComponent._Name
  void				addComponents(nts::IComponent*); // ajout dans l'un des vectors

  void				setComponentInputValue(const std::string &name, nts::Tristate value); // cherche dans _ComponentsInpus en fonction de AComponent._Name et du flag INPUT, set, if not find then std::err
  void				Simulate(); // inverse les clocks puis lauch Compute(0) de _ComponentsOutput
  void				turnOffElectricPower(); // reset les TypePins de tous les vectors à UNDEFINED
  void				Display(); // affiche par ordre de _ComponentsOutput, pas const car utilise std::sort()
  void				Dump() const; // call AComponent.Dump() des vectors
private:
static bool			sortByName(const nts::IComponent* a, const nts::IComponent* b);
};

#endif // CIRCUIT_HPP
