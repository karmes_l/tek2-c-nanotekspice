//
// GenerateComponent.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice/include
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Thu Feb 18 16:33:09 2016 Karmes Lionel
// Last update Sun Feb 28 18:05:06 2016 Karmes Lionel
//


#ifndef GENERATECOMPONENT_HPP
# define GENERATECOMPONENT_HPP

# include "Tristate.hpp"
# include "Component_4008.hpp"
# include "Component_terminal.hpp"
# include "Component_4514.hpp"
# include "Component_4040.hpp"
# include "Component_4013.hpp"
# include "Component_4503.hpp"
# include "Component_4081.hpp"
# include "Component_4071.hpp"
# include "Component_4069.hpp"
# include "Component_4030.hpp"
# include "Component_4011.hpp"
# include "Component_4001.hpp"
# include "Component_input.hpp"
# include "Component_output.hpp"


class   GenerateComponent
{
public:
  GenerateComponent();
  ~GenerateComponent();

  static nts::IComponent*	createComponent(const std::string& type, const std::string& value);
private:
  nts::IComponent*	createInput(const std::string& value) const;
  nts::IComponent*	createClock(const std::string& value) const;
  nts::IComponent*	createFalse(const std::string& value) const;
  nts::IComponent*	createTrue(const std::string& value) const;
  nts::IComponent*	createOutput(const std::string& value) const;
  nts::IComponent*	create4001(const std::string& value) const;
  nts::IComponent*	create4011(const std::string& value) const;
  nts::IComponent*	create4030(const std::string& value) const;
  nts::IComponent*	create4069(const std::string& value) const;
  nts::IComponent*	create4071(const std::string& value) const;
  nts::IComponent*	create4081(const std::string& value) const;
  nts::IComponent*	create4503(const std::string& value) const;
  nts::IComponent*	create4013(const std::string& value) const;
  nts::IComponent*	create4040(const std::string& value) const;
  nts::IComponent*	create4514(const std::string& value) const;
  nts::IComponent*	createTerminal(const std::string& value) const;
  nts::IComponent*	create4008(const std::string& value) const;
};

#endif // GENERATECOMPONENT_HPP
