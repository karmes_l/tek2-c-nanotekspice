//
// Component_input.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 21:58:10 2016 Karmes Lionel
// Last update Sat Feb 13 23:17:13 2016 Karmes Lionel
//

#ifndef COMPONENT_INPUT_HPP
# define COMPONENT_INPUT_HPP

# include "AComponent.hpp"

// a rajouter dans la class si la porté ne pose pa de problème
enum	TypeInput {
  INPUT,
  CLOCK,
  TRUE,
  FALSE
};

class   Component_input : public AComponent
{
  TypeInput	_TypeInput;
public:
  Component_input(const std::string& name, TypeInput typeInput = INPUT); //value = name = nom de la variable (+paramètre entre parenthèse)
  virtual	~Component_input();
  

  TypeInput	getTypeInput() const;

  virtual nts::Tristate Compute(size_t pin_num);
  virtual void			Dump() const; // appelle dump classe mère
  virtual void		initPinsInfo();
};

std::ostream&	operator<<(std::ostream& os, TypeInput typeInput);

#endif // COMPONENT_INPUT_HPP
