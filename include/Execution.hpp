//
// Execution.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice/include
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Sat Feb 13 18:31:11 2016 Karmes Lionel
// Last update Sun Feb 28 23:15:28 2016 Karmes Lionel
//

#ifndef EXECUTION_HPP
# define EXECUTION_HPP

# include "Circuit.hpp"
# include "Parser.hpp"
# include <signal.h>
# include <fstream>

class	Execution
{
  Circuit*	_Circuit;
  static bool		_Sigint;

public:
  Execution(const std::string& filename);
  Execution(Circuit*	circuit);
  ~Execution();

  void				loadInputs(int ac, const char* avs[]); // command line
  void				start(); // regarde si tous les inputs ont leur pin != UNDEFINED, puis appelle readSdtin()
private:
  void				readStdin();
  bool				ChangeInputValue(const std::string& buff);
  bool				Simulate(const std::string& buff);
  bool				Loop(const std::string& buff);
  bool				Display(const std::string& buff);
  void				Dump() const;
  void				setupSIGINT() const ;
  static void			catchSIGINT(int param);
  void				loadInput(const std::string& av); //parse puis, appelle setPinValue() de l'input
  void				checkOutputsLink() const;
  void				loadCircuit(const std::string& filename, nts::IParser* parser);
};

#endif // EXECUTION_HPP
