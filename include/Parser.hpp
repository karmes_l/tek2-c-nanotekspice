//
// Parser.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb 16 14:06:28 2016 Karmes Lionel
// Last update Sun Feb 28 22:58:29 2016 Karmes Lionel
//

#ifndef PARSER_HPP_
# define PARSER_HPP_

# include "cpp_nanotekspice_parser.hpp"
# include "Circuit.hpp"
# include "GenerateComponent.hpp"

namespace       nts_section {
  enum Section {
    NONE,
    CHIPSETS,
    LINKS
  };
}

class	Parser : public nts::IParser {
  std::string	_Input;
  std::string	_Line;
  nts_section::Section	_Section; // vérifie si la secton est valide
  Circuit*	_Circuit;

public:
  Parser();
  virtual ~Parser();

  nts_section::Section	getSection() const;
  Circuit*	getCircuit() const;
  void		setCircuit(Circuit*);

  virtual void	feed(std::string const& input);
  virtual void	parseTree(nts::t_ast_node& root);
  void		deleteTree(nts::t_ast_node* root) const;
  virtual nts::t_ast_node	*createTree();
private:
  void		parseNewlineLink(const nts::t_ast_node& newline);
  void		setSection();
  void			delWhiteSpaceBegin();
  nts::t_ast_node*	createNewline();
  nts::t_ast_node*	createComponent();
  nts::t_ast_node*	createString(const std::string& delim);
  bool			validString(const std::string& str);
  nts::t_ast_node*	createLink();
  nts::t_ast_node*	createLinkEnd();
};

#endif // PARSER_HPP_
