//
// cpp_nanotekspice_parser.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb 16 13:58:01 2016 Karmes Lionel
// Last update Wed Feb 17 22:18:41 2016 Karmes Lionel
//

#ifndef CPP_NANOTEKSPICE_PARSER_HPP
# define CPP_NANOTEKSPICE_PARSER_HPP

# include <string>
# include <vector>

namespace nts
{
  enum class ASTNodeType : int
  {
    DEFAULT = -1,
      NEWLINE = 0,
      SECTION,
      COMPONENT,
      LINK,
      LINK_END,
      STRING
      };
  
typedef struct s_ast_node
{
  s_ast_node(std::vector<struct s_ast_node*> *children)
    : children(children) { }
  std::string	lexeme;
  ASTNodeType	type;
  std::string	value;
  std::vector<struct s_ast_node*> *children;
} t_ast_node;

class IParser
{
public:
  virtual void	feed(std::string const& input) = 0;
  virtual void	parseTree(t_ast_node& root) = 0;
  virtual t_ast_node	*createTree() = 0;
  virtual ~IParser() { }
};
}

#endif //CPP_NANOTEKSPICE_PARSER_HPP
