//
// Component_4011.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 17:58:58 2016 Karmes Lionel
// Last update Sun Feb 21 00:01:02 2016 Karmes Lionel
//

#ifndef COMPONENT_4011_HPP
# define COMPONENT_4011_HPP

# include "AComponent.hpp"

class   Component_4011 : public AComponent
{
public:
  Component_4011(const std::string& name); //value = name = nom de la variable (+paramètre entre parenthèse)
  virtual	~Component_4011();
  

  virtual nts::Tristate Compute(size_t pin_num);
  void		        ComputeEmetor(size_t Y, size_t A, size_t B);
  virtual void			Dump() const;
  virtual void		initPinsInfo();
};

#endif // COMPONENT_4011_HPP
