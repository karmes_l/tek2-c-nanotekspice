//
// Component_terminal.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 17:58:58 2016 Karmes Lionel
// Last update Sun Feb 28 16:00:35 2016 Karmes Lionel
//

#ifndef COMPONENT_TERMINAL_HPP
# define COMPONENT_TERMINAL_HPP

# include "AComponent.hpp"

class   Component_terminal : public AComponent
{
  nts::Tristate	_Mem;
public:
  Component_terminal(const std::string& name); //value = name = nom de la variable (+paramètre entre parenthèse)
  virtual	~Component_terminal();
  

  virtual nts::Tristate Compute(size_t pin_num);
  virtual void			Dump() const;
  virtual void		initPinsInfo();
};

#endif // COMPONENT_TERMINAL_HPP
