//
// Component_4001.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 17:58:58 2016 Karmes Lionel
// Last update Sun Feb 28 22:59:01 2016 Karmes Lionel
//

#ifndef COMPONENT_4030_HPP
# define COMPONENT_4030_HPP

# include "AComponent.hpp"

class   Component_4030 : public AComponent
{
public:
  Component_4030(const std::string& name); //value = name = nom de la variable (+paramètre entre parenthèse)
  virtual	~Component_4030();
  

  virtual nts::Tristate Compute(size_t pin_num);
  void		        ComputeEmetor(size_t Y, size_t A, size_t B);
  virtual void			Dump() const;
  virtual void		initPinsInfo();
};

#endif // COMPONENT_4030_HPP
