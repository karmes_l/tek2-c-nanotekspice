//
// Component_4514.hpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 17:58:58 2016 Karmes Lionel
// Last update Sun Feb 28 13:01:23 2016 Karmes Lionel
//

#ifndef COMPONENT_4514_HPP
# define COMPONENT_4514_HPP

# include "AComponent.hpp"

class   Component_4514 : public AComponent
{
public:
  Component_4514(const std::string& name); //value = name = nom de la variable (+paramètre entre parenthèse)
  virtual	~Component_4514();
  

  virtual nts::Tristate Compute(size_t pin_num);
  virtual void			Dump() const;
  virtual void		initPinsInfo();
};

#endif // COMPONENT_4514_HPP
