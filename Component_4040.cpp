//
// Component_4040.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Sat Feb 27 11:53:40 2016 Karmes Lionel
// Last update Sun Feb 28 13:41:49 2016 Karmes Lionel
//

// Compteur 12bits binaires

#include "Component_4040.hpp"

Component_4040::Component_4040(const std::string& name)
  :AComponent("4040", name, 14), counter(-1) {
  this->_PinsIndex[1] = 0;
  this->_PinsIndex[2] = 1;
  this->_PinsIndex[3] = 2;
  this->_PinsIndex[4] = 3;
  this->_PinsIndex[5] = 4;
  this->_PinsIndex[6] = 5;
  this->_PinsIndex[7] = 6;
  this->_PinsIndex[9] = 7;
  this->_PinsIndex[10] = 8;
  this->_PinsIndex[11] = 9;
  this->_PinsIndex[12] = 10;
  this->_PinsIndex[13] = 11;
  this->_PinsIndex[14] = 12;
  this->_PinsIndex[15] = 13;
  initPinsInfo();
  //ne prend pas de paramètre après le name (si param risque de bug)
}
Component_4040::~Component_4040(void) {
}

// execute TOUT le component des pins EMETOR, les pins EMETOR peuvent s'exécuter plusieurs fois (je n'ai pas set le flag pin._Computed())
nts::Tristate Component_4040::Compute(size_t pinIndex) { // pin_num = pinIndex (afin de pas convertir en PinIndex puis en PinPos recursivement (Compute::Compute() demande un pinIndex)
  int	a;

  AComponent::Compute();
  if (!this->_Latche) {
    if (this->_PinsInfo[this->_PinsIndex[11]].getValue() == nts::TRUE)
      counter = 0;
    else if (this->_PinsInfo[this->_PinsIndex[10]].getValue() == nts::FALSE)
      ++counter;
    if (counter > (2 << 11)) // car il n'y a que 12 bits (4096 valeurs)
      counter = 0;
  }
  this->_Latche = true;
  a = counter;
  this->_PinsInfo[this->_PinsIndex[9]].setValue(a % 2 == 1 ? nts::TRUE : nts::FALSE); // a % 2 == 1 car a peut être = -1 (pour ressembler à la moulinette/simuler la clock)
  this->_PinsInfo[this->_PinsIndex[7]].setValue(((a /= 2) % 2) ? nts::TRUE : nts::FALSE);
  this->_PinsInfo[this->_PinsIndex[6]].setValue((a /= 2) % 2 ? nts::TRUE : nts::FALSE);
  this->_PinsInfo[this->_PinsIndex[5]].setValue((a /= 2) % 2 ? nts::TRUE : nts::FALSE);
  this->_PinsInfo[this->_PinsIndex[3]].setValue((a /= 2) % 2 ? nts::TRUE : nts::FALSE);
  this->_PinsInfo[this->_PinsIndex[2]].setValue((a /= 2) % 2 ? nts::TRUE : nts::FALSE);
  this->_PinsInfo[this->_PinsIndex[4]].setValue((a /= 2) % 2 ? nts::TRUE : nts::FALSE);
  this->_PinsInfo[this->_PinsIndex[13]].setValue((a /= 2) % 2 ? nts::TRUE : nts::FALSE);
  this->_PinsInfo[this->_PinsIndex[12]].setValue((a /= 2) % 2 ? nts::TRUE : nts::FALSE);
  this->_PinsInfo[this->_PinsIndex[14]].setValue((a /= 2) % 2 ? nts::TRUE : nts::FALSE);
  this->_PinsInfo[this->_PinsIndex[15]].setValue((a /= 2) % 2 ? nts::TRUE : nts::FALSE);
  this->_PinsInfo[this->_PinsIndex[1]].setValue((a /= 2) % 2 ? nts::TRUE : nts::FALSE);
  return (this->_PinsInfo[pinIndex].getValue());
}

void			Component_4040::Dump() const {
  std::cout << "Type : " << _Type  << std::endl;
  AComponent::Dump();
}

void		Component_4040::initPinsInfo() {
  this->_PinsInfo[0].setTypePin(EMETOR);
  this->_PinsInfo[1].setTypePin(EMETOR);
  this->_PinsInfo[2].setTypePin(EMETOR);
  this->_PinsInfo[3].setTypePin(EMETOR);
  this->_PinsInfo[4].setTypePin(EMETOR);
  this->_PinsInfo[5].setTypePin(EMETOR);
  this->_PinsInfo[6].setTypePin(EMETOR);
  this->_PinsInfo[7].setTypePin(EMETOR);
  this->_PinsInfo[8].setTypePin(RECEPTOR);
  this->_PinsInfo[9].setTypePin(RECEPTOR);
  this->_PinsInfo[10].setTypePin(EMETOR);
  this->_PinsInfo[11].setTypePin(EMETOR);
  this->_PinsInfo[12].setTypePin(EMETOR);
  this->_PinsInfo[13].setTypePin(EMETOR);
}
