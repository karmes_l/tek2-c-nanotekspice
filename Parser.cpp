//
// Parser.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb 16 14:10:32 2016 Karmes Lionel
// Last update Sun Feb 28 22:58:22 2016 Karmes Lionel
//

#include "Parser.hpp"

// grammar : [DEFAULT]
// DEFAULT : [SECTION] || [NEWLINE] //Note : nts::ASTNodeType SECTION ne sera pas utilis" (une branche SECTION ne me sert à rien)
// SECTION : ".chipsets" || ".links"

// if _Section == nts_section::CHIPSETS
// NEWLINE : [COMPONENT] [STRING]
// else if _Section == nts_section::LINKS
// NEWLINE : [LINK] [LINK]

// COMPONENT = input || clock || output || true || false || 4001 | ...
// STRING = * (sauf espace et tab)
// LINK : [STRING]:[LINK_END] // LOL



Parser::Parser()
  :_Input(""), _Line(""), _Section(nts_section::NONE), _Circuit(new Circuit()) {
}

Parser::~Parser() {
  if (_Circuit)
    delete _Circuit;
}


nts_section::Section	Parser::getSection() const {
  return _Section;
}

Circuit*	Parser::getCircuit() const {
  return _Circuit;
}

void		Parser::setCircuit(Circuit *circuit) {
  _Circuit = circuit;
}

void		Parser::feed(std::string const& input) {
  size_t	pos;

  _Line = input;
  _Input = input;
  delWhiteSpaceBegin();
  if ((pos = _Input.find('#')) != std::string::npos)
    _Input.erase(pos);
}

// maj le _Circuit
void		Parser::parseTree(nts::t_ast_node& root) {
  nts::IComponent*	iComponent;

  (void)root;
  if (root.type == nts::ASTNodeType::NEWLINE
      && (*root.children)[0]->type == nts::ASTNodeType::COMPONENT) {
    if (_Circuit->getComponent((*root.children)[1]->value) != NULL)
      throw std::runtime_error("Component name '" + (*root.children)[1]->value + "' always declared");
    if (!(iComponent = GenerateComponent::createComponent((*root.children)[0]->value, (*root.children)[1]->value)))
      throw std::runtime_error("Component type '" + (*root.children)[0]->value + "' unknow");
    _Circuit->addComponents(iComponent);
  }
  else if (root.type == nts::ASTNodeType::NEWLINE
	   && (*root.children)[0]->type == nts::ASTNodeType::LINK)
    parseNewlineLink(root);
}

void		Parser::deleteTree(nts::t_ast_node* root) const {
  if (root->children) {
    for (size_t i = 0, c = (*root->children).size(); i < c; ++i)
      deleteTree((*root->children)[i]);
    delete root->children;
  }
  delete root;
}

void		Parser::parseNewlineLink(const nts::t_ast_node& newline) {
  nts::t_ast_node	*children;
  std::string	name_a;
  std::string	name_b;
  std::istringstream	iss;
  size_t	pinPos_a;
  size_t	pinPos_b;
  nts::IComponent*	iComponent_a;
  nts::IComponent*	iComponent_b;


    // *link = (*newline.children)[0]
    children = (*newline.children)[0];
    // *string = (*link->children)[0]
    name_a = (*children->children)[0]->value;
    if (!(iComponent_a = _Circuit->getComponent(name_a)))
      throw std::runtime_error("Component name '" + name_a + "' not declared");
    // *link_end = (*link->children)[1]
    iss.str((*children->children)[1]->value);
    iss >> pinPos_a;
    // *link = (*newline.children)[1]
    children = (*newline.children)[1];
    // *string = (*link->children)[0]
    name_b = (*children->children)[0]->value;
    if (!(iComponent_b = _Circuit->getComponent(name_b)))
      throw std::runtime_error("Component name '" + name_b + "' not declared");
    iss.clear();
    // *link_end = (*link->children)[1]
    iss.str((*children->children)[1]->value);
    iss >> pinPos_b;
    iComponent_a->SetLink(pinPos_a, *iComponent_b, pinPos_b);
}

nts::t_ast_node	*Parser::createTree() {
  nts::t_ast_node*	defaut(NULL);

  //ligne vide
  if (_Input == "")
    return NULL;
  // SECTION qui n'est pas un noeud
  if (_Input.find(".chipsets:") == 0 || _Input.find(".links:") == 0)
    setSection();
  else // NEWLINE
    defaut = createNewline();
  delWhiteSpaceBegin();
  if (_Input != "")
    throw std::runtime_error("Error syntax : '" + _Line + "'");
  return defaut;
}

void		Parser::setSection() {
  size_t		pos;
  std::string		section;

  pos = _Input.find_first_of(":") + 1;
  section = _Input.substr(0, pos);
  if (section == ".chipsets:") {
    if (_Section != nts_section::NONE)
      throw std::runtime_error("Chipsets section always set");
    _Section = nts_section::CHIPSETS;
  }
  else if (section == ".links:") {
    if (_Section != nts_section::CHIPSETS)
      throw std::runtime_error("No Chipsets section found before .links or Links section always set");
    _Section = nts_section::LINKS;
  }
  _Input.erase(0, pos);
}

void			Parser::delWhiteSpaceBegin() {
  size_t		pos;

  if (_Input.find_first_of(" \t") == 0) {
    pos = _Input.find_first_not_of(" \t");
    _Input.erase(0, pos);
  }
}

nts::t_ast_node*	Parser::createNewline() {
  std::vector<nts::t_ast_node*>*	children = new std::vector<nts::t_ast_node*>;
  nts::t_ast_node			*newline = new nts::t_ast_node(children);

  try {
    (*children).resize(2);
    if (_Section == nts_section::CHIPSETS) {
      (*newline->children)[0] = createComponent();
      (*newline->children)[1] = createString(" \t");
    }
    else if (_Section == nts_section::LINKS) {
      (*newline->children)[0] = createLink();
      (*newline->children)[1] = createLink();
    }
    else
      throw std::runtime_error("No Chipsets section found before '" + _Line + "'");
    newline->type = nts::ASTNodeType::NEWLINE;
  }
  catch (const std::exception& e) {
    delete children;
    delete newline;
    throw;
  }
  return newline;
}

nts::t_ast_node*	Parser::createComponent() {
  nts::t_ast_node	*component = new nts::t_ast_node(NULL);
  size_t		pos;
  
  try {
    delWhiteSpaceBegin();
    component->type = nts::ASTNodeType::COMPONENT;
    pos = _Input.find_first_of(" \t");
    component->value = _Input.substr(0, pos);
    _Input.erase(0, pos);
  }
  catch (const std::exception& e) {
    delete component;
    throw;
  }
  return component;
}

nts::t_ast_node*	Parser::createString(const std::string& delim) {
  nts::t_ast_node	*string = new nts::t_ast_node(NULL);
  size_t		pos;

  try {
    delWhiteSpaceBegin();
    string->type = nts::ASTNodeType::STRING;
    if (_Input == "" || (pos = _Input.find_first_of(delim)) == 0)
      throw std::runtime_error("Error syntax, line '" + _Line + "' expected name of component before '" + _Input + "'");
    string->value = _Input.substr(0, pos);
    if (!validString(string->value))
      throw std::runtime_error("Error syntax, line '" + _Line + "' invalid String '" + string->value + "'");
    _Input.erase(0, pos);
  }
  catch (const std::exception& e) {
    delete string;
    throw;
  }
  return string;
}

bool			Parser::validString(const std::string& str) {
  if (str == "" || str.at(0) == '.' || str.find_first_of(" \t") != std::string::npos)
    return false;
  return true;
}

nts::t_ast_node*	Parser::createLink() {
  std::vector<nts::t_ast_node*>*	children = new std::vector<nts::t_ast_node*>;
  nts::t_ast_node			*link = new nts::t_ast_node(children);

  try {
    (*children).resize(2);
    delWhiteSpaceBegin();
    (*link->children)[0] = createString(":");
    if (_Input.at(0) == ':')
      _Input.erase(0, 1);  
    else
      throw std::runtime_error("Error syntax, line : '" + _Line + "' expected ':' before '" + _Input + "'");
    (*link->children)[1] = createLinkEnd();
    link->type = nts::ASTNodeType::LINK;
  }
  catch (const std::exception& e) {
    delete children;
    delete link;
    throw;
  }
  return link;
}

nts::t_ast_node*	Parser::createLinkEnd() {
  nts::t_ast_node	*linkEnd = new nts::t_ast_node(NULL);
  size_t		pos;

  try {
    linkEnd->type = nts::ASTNodeType::LINK_END;
    if (_Input == "" || (pos = _Input.find_first_of(" \t")) == 0)
      throw std::runtime_error("Error syntax, line '" + _Line + "' expected link after ':'");
    linkEnd->value = _Input.substr(0, pos);
    _Input.erase(0, pos);
  }
  catch (const std::exception& e) {
    delete linkEnd;
    throw;
  }
  return linkEnd;
}
