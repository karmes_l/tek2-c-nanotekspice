//
// PinInfo.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 22:46:19 2016 Karmes Lionel
// Last update Sat Feb 13 16:39:49 2016 Karmes Lionel
//

#include "PinInfo.hpp"

PinInfo::PinInfo() {
  _Value = nts::UNDEFINED;
  _TypePin = EMETOR;
  _Computed = nts::UNDEFINED;
}

PinInfo::PinInfo(nts::Tristate value, TypePin typePin)
  :_Value(value), _TypePin(typePin) {
}

PinInfo::~PinInfo() {
}

nts::Tristate			PinInfo::getValue() const {
  return _Value;
}

void				PinInfo::setValue(nts::Tristate value) {
  _Value = value;
}

TypePin				PinInfo::getTypePin() const {
  return _TypePin;
}

void				PinInfo::setTypePin(TypePin typePin) {
  _TypePin = typePin;
}

nts::Tristate			PinInfo::getComputed() const {
  return _Computed;
}

void				PinInfo::setComputed(nts::Tristate computed) {
  _Computed = computed;
}

std::ostream&	operator<<(std::ostream& os, TypePin typePin) {
  if (typePin == EMETOR)
    os << "EMETOR";
  else
    os << "RECEPTOR";
  return (os);
}

std::ostream&	operator<<(std::ostream& os, const PinInfo& pinInfo) {
  os << "value : " << pinInfo.getValue() << "  TypePin : " << pinInfo.getTypePin();
  return (os);
}
