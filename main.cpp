//
// main.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Mon Feb  8 23:37:45 2016 Karmes Lionel
// Last update Sun Feb 28 22:29:54 2016 Karmes Lionel
//

#include "nanotekspice.hpp"
#include <iostream>

int	main(int ac, const char **av) {
  if (ac < 2) {
    std::cerr << "Usage : ./nanotekspice file[.nts] [input_name=input_value]" << std::endl;
    return EXIT_FAILURE;
  }
  try {
    Execution	execution(av[1]);
    
    execution.loadInputs(ac, av);
    execution.start();
  }
  catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
