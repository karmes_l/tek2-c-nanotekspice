//
// Component_input.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Tue Feb  9 21:58:49 2016 Karmes Lionel
// Last update Sun Feb 28 22:31:31 2016 Karmes Lionel
//

// INPUT

#include "Component_input.hpp"

Component_input::Component_input(const std::string& name, TypeInput typeInput)
  :AComponent("input", name, 1), _TypeInput(typeInput) {
  this->_PinsIndex[1] = 0;
  initPinsInfo();
  //ne prend pas de paramètre après le name (si param risque de bug)
}
Component_input::~Component_input(void) {
}

TypeInput	Component_input::getTypeInput() const {
  return _TypeInput;
}

nts::Tristate Component_input::Compute(size_t pinIndex) {
  (void)pinIndex;
  return this->_PinsInfo[0].getValue();
}

void			Component_input::Dump() const {
  std::cout << "Type : " << _TypeInput << std::endl; // peut planter car écrit TOUJOURS sur std::cout, à changer si cette forme n'est pas necessaire
  AComponent::Dump();
}

void		Component_input::initPinsInfo() {
  this->_PinsInfo[0].setTypePin(EMETOR);
  if (_TypeInput == TRUE)
    this->_PinsInfo[0].setValue(nts::TRUE);
  if (_TypeInput == FALSE)
    this->_PinsInfo[0].setValue(nts::FALSE);
}



std::ostream&	operator<<(std::ostream& os, TypeInput typeInput) {
  switch (typeInput) {
  case INPUT :
    os << "Input";
    break;
  case CLOCK :
    os << "Clock";
    break;
  case TRUE :
    os << "True";
    break;
  case FALSE :
    os << "False";
    break;
  default:
    os << "TypeInput unknow";
  }
  return (os);
}
