//
// main.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Mon Feb  8 23:37:45 2016 Karmes Lionel
// Last update Wed Feb 17 22:12:31 2016 Karmes Lionel
//

#include "nanotekspice.hpp"
#include <iostream>

int	main(int ac, char *av[]) {
  Component_4001	component_4001("toto"); // nor
  Component_4001	component_4001_2("tata");
  Component_input	component_input[7] = {
    Component_input("a"),
    Component_input("b"),
    Component_input("c"),
    Component_input("d"),
    Component_input("e"),
    Component_input("f"),
    Component_input("g")
  };
  Component_output	component_output("s");
  Circuit		circuit;


  component_4001.Dump();
  component_input[0].setPinInfoValue(component_input[0].getPinIndex(1), nts::FALSE);
  component_input[1].setPinInfoValue(component_input[1].getPinIndex(1), nts::FALSE);
  component_input[2].setPinInfoValue(component_input[2].getPinIndex(1), nts::FALSE);
  component_input[3].setPinInfoValue(component_input[3].getPinIndex(1), nts::FALSE);
  component_input[4].setPinInfoValue(component_input[4].getPinIndex(1), nts::FALSE);
  component_input[5].setPinInfoValue(component_input[4].getPinIndex(1), nts::TRUE);
  component_input[6].setPinInfoValue(component_input[4].getPinIndex(1), nts::FALSE);
  
  component_input[0].SetLink(1, *((nts::IComponent *)&component_4001), 1);
  component_input[1].SetLink(1, *((nts::IComponent *)&component_4001), 2);
  // pin3 = !(FALSE || FALSE) = TRUE
  component_4001.SetLink(3, *((nts::IComponent *)&component_4001), 13);  
  component_input[2].SetLink(1, *((nts::IComponent *)&component_4001), 5);
  component_input[3].SetLink(1, *((nts::IComponent *)&component_4001), 6);
  // pin4 = !(FALSE || FALSE) = TRUE
  component_4001.SetLink(4, *((nts::IComponent *)&component_4001), 12);
  // pin11 = !(TRUE || TRUE) = FALSE
  component_4001.SetLink(11, *((nts::IComponent *)&component_4001), 9);
  component_input[4].SetLink(1, *((nts::IComponent *)&component_4001), 8);
  // pin10 = !(FALSE || FALSE) = TRUE

  // Pas d'ordre dans le linkage
  component_4001_2.SetLink(1, *((nts::IComponent *)&component_4001), 10);
  component_4001_2.SetLink(2, *((nts::IComponent *)&component_input[5]), 1);
  // 4001_2 pin3 = !(TRUE || TRUE) = FALSE
  component_4001_2.SetLink(3, *((nts::IComponent *)&component_4001_2), 5);
  component_4001_2.SetLink(6, *((nts::IComponent *)&component_input[6]), 1);
  // 4001_2 pin4 = !(FALSE || FALSE) = TRUE

  circuit.addComponents((nts::IComponent *)&component_4001);
  circuit.addComponents((nts::IComponent *)&component_4001_2);
  circuit.addComponents((nts::IComponent *)&component_input[0]);
  circuit.addComponents((nts::IComponent *)&component_input[1]);
  circuit.addComponents((nts::IComponent *)&component_input[2]);
  circuit.addComponents((nts::IComponent *)&component_input[3]);
  circuit.addComponents((nts::IComponent *)&component_input[4]);
  circuit.addComponents((nts::IComponent *)&component_input[5]);
  circuit.addComponents((nts::IComponent *)&component_input[6]);
  circuit.addComponents((nts::IComponent *)&component_output);
  circuit.getComponent("s")->SetLink(1, *((nts::IComponent *)&component_4001_2), 4);
  // s pin1 = FALSE
  Execution	execution(&circuit);

  execution.readStdin();
  // execution.Loop();
  // execution.Simulate();
  // execution.Dump();
  // execution.Display();
  // circuit.Simulate();
  // circuit.Dump();
  // circuit.Display();
  (void)ac;
  (void)av;

  return (0);
}
