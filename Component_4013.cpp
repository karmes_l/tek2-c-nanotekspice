//
// Component_4013.cpp for  in /home/karmes_l/Projets/tek2/C++/cpp_nanotekspice
// 
// Made by Karmes Lionel
// Login   <karmes_l@epitech.net>
// 
// Started on  Sat Feb 27 11:53:40 2016 Karmes Lionel
// Last update Sat Feb 27 21:30:52 2016 Karmes Lionel
//

// Dual D-Type Flip-Flop

#include "Component_4013.hpp"

Component_4013::Component_4013(const std::string& name)
  :AComponent("4013", name, 12) {
  this->_PinsIndex[1] = 0;
  this->_PinsIndex[2] = 1;
  this->_PinsIndex[3] = 2;
  this->_PinsIndex[4] = 3;
  this->_PinsIndex[5] = 4;
  this->_PinsIndex[6] = 5;
  this->_PinsIndex[8] = 6;
  this->_PinsIndex[9] = 7;
  this->_PinsIndex[10] = 8;
  this->_PinsIndex[11] = 9;
  this->_PinsIndex[12] = 10;
  this->_PinsIndex[13] = 11;
  initPinsInfo();
  //ne prend pas de paramètre après le name (si param risque de bug)
}
Component_4013::~Component_4013(void) {
}

// execute TOUT le component des pins EMETOR, les pins EMETOR peuvent s'exécuter plusieurs fois (je n'ai pas set le flag pin._Computed())
nts::Tristate Component_4013::Compute(size_t pinIndex) { // pin_num = pinIndex (afin de pas convertir en PinIndex puis en PinPos recursivement (Compute::Compute() demande un pinIndex)
  AComponent::Compute();
  ComputeEmetor(1, 2, 3, 4, 5, 6);
  ComputeEmetor(13, 12, 11, 10, 9, 8);
  return (this->_PinsInfo[pinIndex].getValue());
}

void			Component_4013::ComputeEmetor(size_t Q, size_t NQ, size_t CL, size_t R, size_t D, size_t S) {
  if (this->_PinsInfo[this->_PinsIndex[R]].getValue() == nts::UNDEFINED ||
      this->_PinsInfo[this->_PinsIndex[S]].getValue() == nts::UNDEFINED) {
    this->_PinsInfo[this->_PinsIndex[Q]].setValue(nts::UNDEFINED);
    this->_PinsInfo[this->_PinsIndex[NQ]].setValue(nts::UNDEFINED);
  }
  else {
    if (this->_PinsInfo[this->_PinsIndex[R]].getValue() == nts::TRUE ||
	this->_PinsInfo[this->_PinsIndex[S]].getValue() == nts::TRUE) {
	this->_PinsInfo[this->_PinsIndex[Q]].setValue(nts::FALSE);
	this->_PinsInfo[this->_PinsIndex[NQ]].setValue(nts::FALSE);
      if (this->_PinsInfo[this->_PinsIndex[R]].getValue() == nts::TRUE)
	this->_PinsInfo[this->_PinsIndex[NQ]].setValue(nts::TRUE);
      if (this->_PinsInfo[this->_PinsIndex[S]].getValue() == nts::TRUE)
	this->_PinsInfo[this->_PinsIndex[Q]].setValue(nts::TRUE);
    }
    else if (this->_PinsInfo[this->_PinsIndex[CL]].getValue() == nts::UNDEFINED) {
      this->_PinsInfo[this->_PinsIndex[Q]].setValue(nts::UNDEFINED);
      this->_PinsInfo[this->_PinsIndex[NQ]].setValue(nts::UNDEFINED);
    }
    else {
      if (this->_PinsInfo[this->_PinsIndex[CL]].getValue() == nts::TRUE) {
	this->_PinsInfo[this->_PinsIndex[Q]].setValue(this->_PinsInfo[this->_PinsIndex[D]].getValue());
      }
      this->_PinsInfo[this->_PinsIndex[NQ]].setValue(!this->_PinsInfo[this->_PinsIndex[Q]].getValue());
    }
  }
}

void			Component_4013::Dump() const {
  std::cout << "Type : " << _Type  << std::endl;
  AComponent::Dump();
}

void		Component_4013::initPinsInfo() {
  this->_PinsInfo[0].setTypePin(EMETOR);
  this->_PinsInfo[1].setTypePin(EMETOR);
  this->_PinsInfo[2].setTypePin(RECEPTOR);
  this->_PinsInfo[3].setTypePin(RECEPTOR);
  this->_PinsInfo[4].setTypePin(RECEPTOR);
  this->_PinsInfo[5].setTypePin(RECEPTOR);
  this->_PinsInfo[6].setTypePin(RECEPTOR);
  this->_PinsInfo[7].setTypePin(RECEPTOR);
  this->_PinsInfo[8].setTypePin(RECEPTOR);
  this->_PinsInfo[9].setTypePin(RECEPTOR);
  this->_PinsInfo[10].setTypePin(EMETOR);
  this->_PinsInfo[11].setTypePin(EMETOR);
}
